use <roundedcube.scad>

nbPlank_topDeck = 9;


module plank(length) {
  roundedcube([13.8, length, 2.5], false, 0.5, "y");
}

module floor(length, nbPlank) {
  for(i= [0:nbPlank])
    translate([14*i, 0, 0])
      plank(length);
}

module deck(length, nbPlank, height) {
  translate([0, 0, height])
    floor(length, nbPlank);

  // legs
  translate([0, 0, 0])
    roundedcube([9, 9, height], false, 0.5, "z");

  translate([14*(nbPlank+1), 0, 0])
    mirror([1, 0, 0])
    roundedcube([9, 9, height], false, 0.5, "z");

  translate([0, length, 0])
    mirror([0, 1, 0])
    roundedcube([9, 9, height], false, 0.5, "z");

  translate([14*(nbPlank+1), length, 0])
    mirror([1, 1, 0])
    roundedcube([9, 9, height], false, 0.5, "z");
}

module brickwall(nby, nbz){
  nbBricks=nby*nbz;
  bricks=rands(0, 3, nbBricks);
  for(i= [0:nbBricks-1]) {
    let (y=i%nby, z=floor(i/nby)) {
      brick=ceil(bricks[i]);
      translate([0, 18*y, 8*z]) {
        if(brick == 1){
          color("DarkRed")
            cube([2, 17, 7]);
        }
        else if (brick == 2){
          color("FireBrick")
            cube([2, 17, 7]);
        }
        else {
          color("black")
            cube([2, 17, 7]);
        }
      }
    }
  }
}

module staire(length, depth) {
  translate([0, 0, depth-3.8])
    roundedcube([depth, length, 3.8], false, 0.2, "y");

  translate([depth-3.8,0,0])
    roundedcube([3.8, length, depth - 3.8], false, 0.2, "y");
}

module staires(length, depth, nbStaires) {
  for(i= [0:(nbStaires-1)])
    translate([depth*i, 0, depth*(nbStaires-i-1)])
      staire(length, depth);
}



// house
difference() {
  union() {
    // fundation
    c="lightGrey";
    mirror([1, 0, 0]){
      color(c)
        cube([915, 1828, 305]);
    }

    // walls
    translate([0, 0, 91]){

      brickwall(101, 27);

      translate([-915, 0, 0])
        mirror([1, 0, 0])
        brickwall(101, 27);

      mirror([0, 1, 0])
        rotate([0, 0, 90])
        brickwall(51, 27);

      translate([0, 1828, 0])
        rotate([0, 0, 90])
        brickwall(51, 27);
    }
  }
  translate([-10, 91, 95])
    cube([20, 152, 179]);
}



// deck
color("BurlyWood", 1.0) {
  // upper deck
  deck(244, nbPlank_topDeck, 104);

  // lower deck
  translate([14*(nbPlank_topDeck+1), 0, 0])
    deck(426, 35, 27);


  // platform
  translate([0, 244, 0]) {
    deck(183, nbPlank_topDeck, 104-21.5);

  // staires
    translate([(nbPlank_topDeck+1)*14, 0, 27])
      staires(122, 23.5, 3);
  }

}


